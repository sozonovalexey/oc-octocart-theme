# OctoCart Theme

Basic Bootstrap theme for eCommerce plugin [OctoCart](http://octobercms.com/plugin/xeor-octocart).

## Installation

`$ git clone git@bitbucket.org:sozonovalexey/oc-octocart-theme.git` to themes folder.

Rename cloned folder to `octocart`.

## Activating the theme

`$ php artisan theme:use octocart`

## Customizing

**CSS**

`assets/css/custom.css`

**JS**

`assets/javascript/custom.js`

### Other files

`README.md` may be deleted.

`version.yaml` may be deleted.

`theme.yaml` should be emptied (not deleted).

`assets/images/theme-preview.png` should either be deleted or replaced with a screenshot of your final theme.

Consult the [official documentation](https://octobercms.com/docs/themes/development) for more information on theme development.
